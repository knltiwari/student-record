from django.shortcuts import render
from rest_framework.viewsets import ModelViewSet
from Students.models import StudentRecord
from Students.serializers import StudentSerializer
# Create your views here.
class StudentCRUD(ModelViewSet):
      queryset= StudentRecord.objects.all()
      serializer_class=StudentSerializer
