from Students.models import StudentRecord
from rest_framework.serializers import ModelSerializer
class StudentSerializer(ModelSerializer):
    class Meta:
        model=StudentRecord
        fields='__all__'
